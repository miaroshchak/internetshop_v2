﻿using System.Collections.Generic;
using InternetShop.Service.Models.ModifyModels;

namespace InternetShop.WebApi.Controllers
{
    using Service.Interfaces;
    using System.Web.Http;


    [RoutePrefix("api/category")]
    public class CategoryController : ApiController
    {
        private readonly ICategoryService _categoryService;

        public CategoryController(ICategoryService categoryService)
        {
            _categoryService = categoryService;
        }



        [Route("")]
        [HttpPost]
        public IHttpActionResult Create(CategoryCreateModel model)
        {
            _categoryService.Create(model);
            return Ok();
        }

        [Route("")]
        [HttpGet]
        public IHttpActionResult GetCategory(int id)
        {
            _categoryService.GetAsync(id);
            return Ok();
        }


    }
}