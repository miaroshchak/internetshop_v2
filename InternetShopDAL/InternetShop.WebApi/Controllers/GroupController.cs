﻿using InternetShop.Service.Models.ModifyModels;

namespace InternetShop.WebApi.Controllers
{
    using Service.Interfaces;
    using System.Web.Http;


    [RoutePrefix("api/group")]
    public class GroupController : ApiController
    {
        private readonly IGroupService _groupService;

        public GroupController(IGroupService groupService)
        {
            _groupService = groupService;
        }


        [Route("")]
        [HttpPost]
        public IHttpActionResult Create(GroupCreateModel model)
        {
            _groupService.Create(model);
            return Ok();
        }

        [Route("")]
        [HttpGet]
        public IHttpActionResult GetCategory(int id)
        {
            _groupService.GetAsync(id);
            return Ok();
        }


    }
}