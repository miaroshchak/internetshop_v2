﻿namespace InternetShop.DAL.UnitOfWork
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Threading.Tasks;
    using Model;

    public class UnitOfWorkAsync : IUnitOfWorkAsync
    {
        private readonly DbContext _context;
        private readonly Dictionary<Type, IRepository> _innerCache = new Dictionary<Type, IRepository>();

        public UnitOfWorkAsync()
        {
            _context = new DataBaseContext();
        }

        public IGenericRepository<TEntity> GetRepository<TEntity>() where TEntity : class
        {
            if (!_innerCache.ContainsKey(typeof(TEntity)))
            {
                _innerCache.Add(typeof(TEntity), new GenericRepository<TEntity>(_context));
            }

            return (GenericRepository<TEntity>)_innerCache[typeof(TEntity)];
        }

        public Task<int> SaveChanges()
        {
            return _context.SaveChangesAsync();
        }

        public void Dispose()
        {
            _context.Dispose();
        }
    }
}
