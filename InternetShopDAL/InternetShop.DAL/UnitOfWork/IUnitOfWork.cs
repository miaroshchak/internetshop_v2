﻿namespace InternetShop.DAL.UnitOfWork
{
    using System;
    using System.Threading.Tasks;
    using Model;

    public interface IUnitOfWorkAsync : IDisposable
    {
        IGenericRepository<TEntity> GetRepository<TEntity>() where TEntity : class;

        Task<int> SaveChanges();
    }
}
