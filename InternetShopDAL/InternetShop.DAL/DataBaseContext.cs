﻿using InternetShop.DAL.Configuration;
using InternetShop.DAL.Model.Entity;
using System.Data.Entity;

namespace InternetShop.DAL
{
    public class DataBaseContext : DbContext
    {
        public DbSet<UserPublic> UserPublics { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<Group> Groups { get; set; }
        public DbSet<Price> Prices { get; set; }
        public DbSet<PriceType> PriceTypes { get; set; }
        public DbSet<Сarrier> Carriers { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Selling> Sellings { get; set; }
        public DbSet<OrderDetail> OrderDetails { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<Address> Adresses { get; set; }
        public DbSet<SellingDetail> SellingDetails { get; set; }

        public DataBaseContext() : base("name=InternetShop")
        {
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new GroupConfiguration());
            modelBuilder.Configurations.Add(new CategoryConfiguration());
            modelBuilder.Configurations.Add(new PriceTypeConfiguration());
            modelBuilder.Configurations.Add(new OrderConfiguration());
            modelBuilder.Configurations.Add(new SellingConfiguration());
            modelBuilder.Configurations.Add(new СarrierConfiguration());
            modelBuilder.Configurations.Add(new UserPublicConfiguration());
            modelBuilder.Configurations.Add(new UserConfiguration());
            modelBuilder.Configurations.Add(new SellingDetailConfiguration());
            modelBuilder.Configurations.Add(new ProductConfiguration());
            modelBuilder.Configurations.Add(new PriceConfiguration());
            modelBuilder.Configurations.Add(new OrderDetailConfiguration());
            modelBuilder.Configurations.Add(new AddressConfiguration());
        }
    }
}
