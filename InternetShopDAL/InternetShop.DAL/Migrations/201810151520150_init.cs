namespace InternetShop.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class init : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Addresses",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        City = c.String(),
                        Street = c.String(),
                        Building = c.String(),
                        Apartment = c.String(),
                        LastModified = c.DateTime(nullable: false),
                        Status = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Сarrier",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Address = c.String(),
                        LastModified = c.DateTime(nullable: false),
                        Status = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Categories",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        LastModified = c.DateTime(nullable: false),
                        Status = c.Int(nullable: false),
                        Group_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Groups", t => t.Group_Id, cascadeDelete: true)
                .Index(t => t.Group_Id);
            
            CreateTable(
                "dbo.Groups",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        LastModified = c.DateTime(nullable: false),
                        Status = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Products",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Article = c.String(),
                        ShortName = c.String(),
                        FullName = c.String(),
                        Description = c.String(),
                        LastModified = c.DateTime(nullable: false),
                        Status = c.Int(nullable: false),
                        Group_Id = c.Int(),
                        Category_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Groups", t => t.Group_Id)
                .ForeignKey("dbo.Categories", t => t.Category_Id, cascadeDelete: true)
                .Index(t => t.Group_Id)
                .Index(t => t.Category_Id);
            
            CreateTable(
                "dbo.OrderDetails",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Quantity = c.Int(nullable: false),
                        Cost = c.Double(nullable: false),
                        Discount = c.Double(nullable: false),
                        LastModified = c.DateTime(nullable: false),
                        Status = c.Int(nullable: false),
                        Orders_Id = c.Int(nullable: false),
                        Product_Id = c.Int(),
                        UserPublic_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Orders", t => t.Orders_Id, cascadeDelete: true)
                .ForeignKey("dbo.Products", t => t.Product_Id)
                .ForeignKey("dbo.UserPublics", t => t.UserPublic_Id)
                .Index(t => t.Orders_Id)
                .Index(t => t.Product_Id)
                .Index(t => t.UserPublic_Id);
            
            CreateTable(
                "dbo.Orders",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CreateDate = c.DateTime(nullable: false),
                        TotalSuma = c.Double(nullable: false),
                        LastModified = c.DateTime(nullable: false),
                        Status = c.Int(nullable: false),
                        Address_Id = c.Int(),
                        UserPublic_Id = c.Int(),
                        Сarriers_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Addresses", t => t.Address_Id)
                .ForeignKey("dbo.UserPublics", t => t.UserPublic_Id)
                .ForeignKey("dbo.Сarrier", t => t.Сarriers_Id)
                .Index(t => t.Address_Id)
                .Index(t => t.UserPublic_Id)
                .Index(t => t.Сarriers_Id);
            
            CreateTable(
                "dbo.UserPublics",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FirstName = c.String(),
                        LastName = c.String(),
                        PhoneNumber = c.Int(nullable: false),
                        Email = c.String(),
                        Login = c.String(),
                        Password = c.String(),
                        LastModified = c.DateTime(nullable: false),
                        Status = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Prices",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Cost = c.Double(nullable: false),
                        LastModified = c.DateTime(nullable: false),
                        Status = c.Int(nullable: false),
                        PriceType_Id = c.Int(nullable: false),
                        Product_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.PriceTypes", t => t.PriceType_Id, cascadeDelete: true)
                .ForeignKey("dbo.Products", t => t.Product_Id)
                .Index(t => t.PriceType_Id)
                .Index(t => t.Product_Id);
            
            CreateTable(
                "dbo.PriceTypes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        LastModified = c.DateTime(nullable: false),
                        Status = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.SellingDetails",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Quantity = c.Int(nullable: false),
                        Cost = c.Double(nullable: false),
                        Discount = c.Double(nullable: false),
                        LastModified = c.DateTime(nullable: false),
                        Status = c.Int(nullable: false),
                        Product_Id = c.Int(),
                        Selling_Id = c.Int(nullable: false),
                        UserPublic_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Products", t => t.Product_Id)
                .ForeignKey("dbo.Sellings", t => t.Selling_Id, cascadeDelete: true)
                .ForeignKey("dbo.UserPublics", t => t.UserPublic_Id)
                .Index(t => t.Product_Id)
                .Index(t => t.Selling_Id)
                .Index(t => t.UserPublic_Id);
            
            CreateTable(
                "dbo.Sellings",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CreateDate = c.DateTime(nullable: false),
                        SellingDate = c.DateTime(nullable: false),
                        TotalSuma = c.Double(nullable: false),
                        Payment = c.Int(nullable: false),
                        PaymentDate = c.DateTime(nullable: false),
                        LastModified = c.DateTime(nullable: false),
                        Status = c.Int(nullable: false),
                        Address_Id = c.Int(),
                        UserPublic_Id = c.Int(),
                        Сarriers_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Addresses", t => t.Address_Id)
                .ForeignKey("dbo.UserPublics", t => t.UserPublic_Id)
                .ForeignKey("dbo.Сarrier", t => t.Сarriers_Id)
                .Index(t => t.Address_Id)
                .Index(t => t.UserPublic_Id)
                .Index(t => t.Сarriers_Id);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FirstName = c.String(),
                        LastName = c.String(),
                        PhoneNumber = c.Int(nullable: false),
                        Email = c.String(),
                        Login = c.String(),
                        Password = c.String(),
                        LastModified = c.DateTime(nullable: false),
                        Status = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.SellingDetails", "UserPublic_Id", "dbo.UserPublics");
            DropForeignKey("dbo.Sellings", "Сarriers_Id", "dbo.Сarrier");
            DropForeignKey("dbo.Sellings", "UserPublic_Id", "dbo.UserPublics");
            DropForeignKey("dbo.SellingDetails", "Selling_Id", "dbo.Sellings");
            DropForeignKey("dbo.Sellings", "Address_Id", "dbo.Addresses");
            DropForeignKey("dbo.SellingDetails", "Product_Id", "dbo.Products");
            DropForeignKey("dbo.Prices", "Product_Id", "dbo.Products");
            DropForeignKey("dbo.Prices", "PriceType_Id", "dbo.PriceTypes");
            DropForeignKey("dbo.OrderDetails", "UserPublic_Id", "dbo.UserPublics");
            DropForeignKey("dbo.OrderDetails", "Product_Id", "dbo.Products");
            DropForeignKey("dbo.Orders", "Сarriers_Id", "dbo.Сarrier");
            DropForeignKey("dbo.Orders", "UserPublic_Id", "dbo.UserPublics");
            DropForeignKey("dbo.OrderDetails", "Orders_Id", "dbo.Orders");
            DropForeignKey("dbo.Orders", "Address_Id", "dbo.Addresses");
            DropForeignKey("dbo.Products", "Category_Id", "dbo.Categories");
            DropForeignKey("dbo.Products", "Group_Id", "dbo.Groups");
            DropForeignKey("dbo.Categories", "Group_Id", "dbo.Groups");
            DropIndex("dbo.Sellings", new[] { "Сarriers_Id" });
            DropIndex("dbo.Sellings", new[] { "UserPublic_Id" });
            DropIndex("dbo.Sellings", new[] { "Address_Id" });
            DropIndex("dbo.SellingDetails", new[] { "UserPublic_Id" });
            DropIndex("dbo.SellingDetails", new[] { "Selling_Id" });
            DropIndex("dbo.SellingDetails", new[] { "Product_Id" });
            DropIndex("dbo.Prices", new[] { "Product_Id" });
            DropIndex("dbo.Prices", new[] { "PriceType_Id" });
            DropIndex("dbo.Orders", new[] { "Сarriers_Id" });
            DropIndex("dbo.Orders", new[] { "UserPublic_Id" });
            DropIndex("dbo.Orders", new[] { "Address_Id" });
            DropIndex("dbo.OrderDetails", new[] { "UserPublic_Id" });
            DropIndex("dbo.OrderDetails", new[] { "Product_Id" });
            DropIndex("dbo.OrderDetails", new[] { "Orders_Id" });
            DropIndex("dbo.Products", new[] { "Category_Id" });
            DropIndex("dbo.Products", new[] { "Group_Id" });
            DropIndex("dbo.Categories", new[] { "Group_Id" });
            DropTable("dbo.Users");
            DropTable("dbo.Sellings");
            DropTable("dbo.SellingDetails");
            DropTable("dbo.PriceTypes");
            DropTable("dbo.Prices");
            DropTable("dbo.UserPublics");
            DropTable("dbo.Orders");
            DropTable("dbo.OrderDetails");
            DropTable("dbo.Products");
            DropTable("dbo.Groups");
            DropTable("dbo.Categories");
            DropTable("dbo.Сarrier");
            DropTable("dbo.Addresses");
        }
    }
}
