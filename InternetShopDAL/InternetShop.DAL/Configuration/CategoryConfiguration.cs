﻿using InternetShop.DAL.Model.Entity;

namespace InternetShop.DAL.Configuration
{
    using System.Data.Entity.ModelConfiguration;

    public class CategoryConfiguration : EntityTypeConfiguration<Category>
    {
        public CategoryConfiguration()
        {
            HasMany(p => p.Products)
                .WithRequired(c => c.Category);
        }

    }
}