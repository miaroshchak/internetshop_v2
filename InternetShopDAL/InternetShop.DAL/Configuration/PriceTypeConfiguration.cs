﻿namespace InternetShop.DAL.Configuration
{
    using Model.Entity;
    using System.Data.Entity.ModelConfiguration;

    public class PriceTypeConfiguration : EntityTypeConfiguration<PriceType>
    {
        public PriceTypeConfiguration()
        {
            HasMany(p => p.Prices)
                .WithRequired(c => c.PriceType);
        }
    }
}