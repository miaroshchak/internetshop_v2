﻿using InternetShop.DAL.Model.Entity;

namespace InternetShop.DAL.Configuration
{
    using System.Data.Entity.ModelConfiguration;

    public class GroupConfiguration : EntityTypeConfiguration<Group>
    {
        public GroupConfiguration()
        {
            HasMany(p => p.Categories)
                .WithRequired(c => c.Group);
        }

    }
}