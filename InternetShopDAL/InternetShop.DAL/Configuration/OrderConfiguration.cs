﻿using InternetShop.DAL.Model.Entity;

namespace InternetShop.DAL.Configuration
{
    using System.Data.Entity.ModelConfiguration;

    public class OrderConfiguration : EntityTypeConfiguration<Order>
    {
        public OrderConfiguration()
        {
            HasMany(p => p.OrderDetail)
                .WithRequired(c => c.Orders);
        }

    }
}