﻿using InternetShop.DAL.Model.Entity;

namespace InternetShop.DAL.Configuration
{
    using System.Data.Entity.ModelConfiguration;

    public class UserPublicConfiguration : EntityTypeConfiguration<UserPublic>
    {
        public UserPublicConfiguration()
        {
        }
    }
}