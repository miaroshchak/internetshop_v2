﻿using InternetShop.DAL.Model.Entity;

namespace InternetShop.DAL.Configuration
{
    using System.Data.Entity.ModelConfiguration;

    public class SellingConfiguration : EntityTypeConfiguration<Selling>
    {
        public SellingConfiguration()
        {
            HasMany(p => p.SellingDetail)
                .WithRequired(c => c.Selling);
        }

    }
}