﻿namespace InternetShop.DAL.Model
{
    using System;
    using Enum;

    public class BaseElement
    {
        public int Id { get; set; }

        public DateTime LastModified { get; set; }

        public Status Status { get; set; }
    }
}
