﻿namespace InternetShop.DAL.Model
{
    using System.Data.Entity;
    using System.Threading.Tasks;

    public class GenericRepository<TEntity> : IGenericRepository<TEntity> where TEntity : class
    {
        private readonly DbContext _context;

        public GenericRepository(DbContext context)
        {
            _context = context;
        }

        public async Task<TEntity> GetAsync<TKey>(TKey key)
        {
            return await _context.Set<TEntity>().FindAsync(key);
        }

        public void Add(TEntity item)
        {
            _context.Set<TEntity>().Add(item);
        }
        
        public void Remove(TEntity item)
        {
            _context.Set<TEntity>().Remove(item);
        }
    }
}
