﻿namespace InternetShop.DAL.Model.Enum
{
    public enum DeliveryStatus
    {
        New,
        Processed,
        Сanceled,
        Delete,
        Done,
        Hold
    }
}