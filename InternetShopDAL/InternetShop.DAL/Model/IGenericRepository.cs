﻿namespace InternetShop.DAL.Model
{
    using System.Threading.Tasks;

    public interface IRepository { }

    public interface IGenericRepository<TEntity> : IRepository where TEntity : class
    {
        void Add(TEntity item);

        Task<TEntity> GetAsync<TKey>(TKey key);

        void Remove(TEntity item);
    }
}
