﻿namespace InternetShop.DAL.Model.Entity
{
    using System;
    using System.Collections.Generic;

    public class Order : BaseElement
    {
        public Order()
        {
            OrderDetail = new List<OrderDetail>();
        }

        public virtual ICollection<OrderDetail> OrderDetail { get; set; }

        public DateTime CreateDate { get; set; }

        public Address Address { get; set; }

        public UserPublic UserPublic { get; set; }

        public Сarrier Сarriers { get; set; }

        public double TotalSuma { get; set; }

    }
}
