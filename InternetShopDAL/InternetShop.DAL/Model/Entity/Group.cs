﻿namespace InternetShop.DAL.Model.Entity
{
    using System.Collections.Generic;

    public class Group : BaseElement
    {
        public Group()
        {
            Categories = new List<Category>();
        }

        public string Name { get; set; }

        public ICollection<Category> Categories { get; set; }
    }
}
