﻿namespace InternetShop.DAL.Model.Entity
{
    public class Price : BaseElement
    {

        public virtual Product Product { get; set; }

        public double Cost { get; set; }

        public virtual PriceType PriceType { get; set; }

    }
}
