﻿namespace InternetShop.DAL.Model.Entity
{
    public class OrderDetail : BaseElement
   {
       public virtual Product Product { get; set; }

       public int Quantity { get; set; }

       public double Cost { get; set; }

       public double Discount { get; set; }

       public Order Orders { get; set; }

       public UserPublic UserPublic { get; set; }

   }
}
