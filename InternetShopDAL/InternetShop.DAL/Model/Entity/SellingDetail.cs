﻿namespace InternetShop.DAL.Model.Entity
{
    public class SellingDetail : BaseElement
   {
       public virtual Product Product { get; set; }

       public int Quantity { get; set; }

       public double Cost { get; set; }

       public double Discount { get; set; }

       public virtual Selling Selling { get; set; }

       public virtual UserPublic UserPublic { get; set; }

   }
}
