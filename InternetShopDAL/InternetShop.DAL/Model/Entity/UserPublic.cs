﻿namespace InternetShop.DAL.Model.Entity
{
    public class UserPublic : BaseElement
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public int PhoneNumber { get; set; }

        public string Email { get; set; }

        public string Login { get; set; }

        public string Password { get; set; }

    }
}
