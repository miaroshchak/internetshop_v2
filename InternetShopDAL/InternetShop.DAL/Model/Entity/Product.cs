﻿namespace InternetShop.DAL.Model.Entity
{
    public class Product : BaseElement
    {
       public string Article { get; set; }

        public string ShortName { get; set; }

        public string FullName { get; set; }

        public string Description { get; set; }

        //public int CategoryId { get; set; }

        //public int GroupId { get; set; }

        public virtual Group Group { get; set; }
               
        public virtual Category Category { get; set; }
    }
}
