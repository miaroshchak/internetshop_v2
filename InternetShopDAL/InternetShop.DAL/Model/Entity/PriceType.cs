﻿namespace InternetShop.DAL.Model.Entity
{
    using System.Collections.Generic;

    public class PriceType : BaseElement
    {
        public PriceType(ICollection<Price> prices)
        {
            Prices = prices;
        }

        public string Name { get; set; }

        public virtual ICollection<Price> Prices { get; set; }

    }
}
