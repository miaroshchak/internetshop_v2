﻿namespace InternetShop.DAL.Model.Entity
{
    public class Address : BaseElement
    {
        public string City { get; set; }

        public string Street { get; set; }

        public string Building { get; set; }

        public string Apartment { get; set; }

    }
}
