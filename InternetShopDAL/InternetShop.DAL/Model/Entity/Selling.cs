﻿using InternetShop.DAL.Model.Enum;

namespace InternetShop.DAL.Model.Entity
{
    using System;
    using System.Collections.Generic;

    public class Selling : BaseElement
    {
        public Selling()
        {
            SellingDetail = new List<SellingDetail>();
        }

        public virtual ICollection<SellingDetail> SellingDetail { get; set; }

        public DateTime CreateDate { get; set; }

        public DateTime SellingDate { get; set; }

        public Address Address { get; set; }

        public UserPublic UserPublic { get; set; }

        public Сarrier Сarriers { get; set; }

        public double TotalSuma { get; set; }

        public Payment Payment { get; set; }

        public DateTime PaymentDate { get; set; }

    }
}
