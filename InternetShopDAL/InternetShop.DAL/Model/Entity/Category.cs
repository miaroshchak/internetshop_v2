﻿namespace InternetShop.DAL.Model.Entity
{
    using System.Collections.Generic;

    public class Category : BaseElement
    {
        public Category()
        {
            Products = new List<Product>();
        }

        public string Name { get; set; }

        public virtual Group Group { get; set; }

        public virtual ICollection<Product> Products { get; set; }
    }
}
