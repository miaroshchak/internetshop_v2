﻿using System.Collections.Generic;

namespace InternetShop.Service.Interfaces
{
    using Models.ModifyModels;
    using Models.ViewModels;
    using System.Threading.Tasks;

    public interface ICategoryService
    {
        Task Create(CategoryCreateModel category);

        Task<CategoryViewModel> GetAsync(int id);

        //IEnumerable<CategoryViewModel> GetAsync();

        void Remove(int id);
    }
}
