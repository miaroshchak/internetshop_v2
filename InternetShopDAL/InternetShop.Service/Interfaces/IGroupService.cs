﻿namespace InternetShop.Service.Interfaces
{
    using Models.ModifyModels;
    using Models.ViewModels;
    using System.Threading.Tasks;

    public interface IGroupService
    {
        Task Create(GroupCreateModel category);

        Task<GroupViewModel> GetAsync(int id);

        void Remove(int id);
    }
}
