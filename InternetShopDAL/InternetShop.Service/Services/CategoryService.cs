﻿using System.Collections.Generic;

namespace InternetShop.Service.Services
{
    using System.Threading.Tasks;
    using DAL.Model.Entity;
    using DAL.UnitOfWork;
    using Interfaces;
    using Models.ModifyModels;
    using Models.ViewModels;

    public class CategoryService : ICategoryService
    {
        protected readonly IUnitOfWorkAsync _unitOfWork;

        public CategoryService(IUnitOfWorkAsync unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task Create(CategoryCreateModel model)
        {
            var repository = _unitOfWork.GetRepository<Category>();

            var category = new Category
            {
                Name = model.Name
            };

            repository.Add(category);

            await _unitOfWork.SaveChanges();
        }

        public async Task<CategoryViewModel> GetAsync(int id)
        {
            var repository = _unitOfWork.GetRepository<Category>();

            var model = await repository.GetAsync(id);

            var category = new CategoryViewModel
            {
                Name = model.Name
            };

            return category;
        }

        public async void Remove(int id)
        {
            var repository = _unitOfWork.GetRepository<Category>();

            var item = await repository.GetAsync(id);

            repository.Remove(item);
        }
    }
}
