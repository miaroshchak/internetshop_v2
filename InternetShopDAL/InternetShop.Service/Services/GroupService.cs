﻿namespace InternetShop.Service.Services
{
    using System.Threading.Tasks;
    using DAL.Model.Entity;
    using DAL.UnitOfWork;
    using Interfaces;
    using Models.ModifyModels;
    using Models.ViewModels;

    public class GroupService : IGroupService
    {
        protected readonly IUnitOfWorkAsync _unitOfWork;

        public GroupService(IUnitOfWorkAsync unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task Create(GroupCreateModel model)
        {
            var repository = _unitOfWork.GetRepository<Group>();

            var group = new Group
            {
                Name = model.Name
            };

            repository.Add(group);

            await _unitOfWork.SaveChanges();
        }

        public async Task<GroupViewModel> GetAsync(int id)
        {
            var repository = _unitOfWork.GetRepository<Group>();

            var model = await repository.GetAsync(id);

            var group = new GroupViewModel
            {
                Name = model.Name
            };

            return group;
        }

        public async void Remove(int id)
        {
            var repository = _unitOfWork.GetRepository<Group>();

            var item = await repository.GetAsync(id);

            repository.Remove(item);
        }
    }
}
